; Source: https://www.reddit.com/r/AutoHotkey/comments/7bninp/macro_for_snipping_tool/

^!t::Sb_Cmder()					;CAPSLOCK + F1 OPENS SNIPPING TOOL/SAVES CURRENT SNIP

Sb_Cmder() 
{
	If WinExist("Cmder")
	{
		IfWinActive, Cmder
		{
		} 
		else 
		{
			Fn_ActivateCmder()
		}
	} else
	{
		Fn_OpenCmder()
	}
}

Fn_OpenCmder() {
	RunWait C:\Tools\Cmder\Cmder.exe
	WinWait, Cmder
	WinActivate, Cmder
}

Fn_ActivateCmder() {
	WinActivate, Cmder
	WinWaitActive, Cmder
}