# AutoHotKey Utilities

For autostarting your scripts place them here:

```
%appdata%\Microsoft\Windows\Start Menu\Programs\Startup
```

## Hide Desktop Icons

Thi script hides the desktop icons by using the win+h key, click again to show

## Youtube-dl

This script allows to control youtube tab on chrome. This has a prerequisite of the youtube-dl plugin.
Find it [here](https://github.com/rg3/youtube-dl/blob/master/README.md#readme)

- ctrl+alt+t -> toggles
- ctrl+alt+n -> next song
- ctrl+alt+b -> previous song
- ctrl+alt+c -> download current song
- ctrl+alt+x -> search current clipboard contents with youtube-dl and download the best result
