; Source: https://www.reddit.com/r/AutoHotkey/comments/7bninp/macro_for_snipping_tool/
~^PrintScreen::Sb_Snip()					;CAPSLOCK + F1 OPENS SNIPPING TOOL/SAVES CURRENT SNIP

Sb_Snip() {
	If WinExist("Snipping Tool")
	{
		IfWinActive, Snipping Tool
		{
			Fn_SaveSnip()
		} else Fn_ActivateStartSnip()
	} else Fn_OpenSnippingTool()
	return

}

Fn_SaveSnip() {
	Send ^s
	Sleep, 450
	FormatTime, CurrentDate,, yyyyMMddhhmmss
	SendInput %A_Desktop%
	SendInput \
	SendInput %CurrentDate%
	Sleep, 200
	Send !s
	Sleep, 200
	WinClose, Snipping Tool
}

Fn_OpenSnippingTool() {
	Run %A_WinDir%\System32\SnippingTool.exe
	WinWait, Snipping Tool
	WinActivate, Snipping Tool
	Send !n
	Send r
}

Fn_ActivateStartSnip() {
	WinActivate, Snipping Tool
	WinWaitActive, Snipping Tool
	Send !n
	Send r
}

; ~^PrintScreen::

; Run %A_WinDir%\System32\SnippingTool.exe

; WinWait, Snipping Tool
; WinActivate, Snipping Tool

; ; Sleep, 5
; ; IfWinExist, ahk_class Snipping Tool
; ; WinActivate ahk_class Snipping Tool ; use the window found above
; ; WinWaitActive, ahk_exe SnippingTool.exe

; ; Sleep, 10
; ; sendinput !{n}
; ; Sleep, 150
; Send !n
; ; Sleep, 100
; Send r
