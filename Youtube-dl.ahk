﻿; Download the Current Playing track from Youtube
^!c::YoutubeDownload()

YoutubeDownload()
{
  results := SendToYoutube("")
  found := results[1]
  title := results[2]
  address := results[3]

  if(found)
  {
    if(InStr(address, "list", false) > 0)
    {
      title := StrReplace(title, "- YouTube - Google Chrome", "")
      ; MsgBox, , Title, "From List", 1
      YoutubeDownloadFromTitle(title)
    }
    Else
    {
      ; MsgBox, , Title, "From Address", 1
      YoutubeDownloadFromAddress(address)
    }
  }

}

YoutubeDownloadFromAddress(address)
{
  Run, youtube-dl "%address%",,Minimize
}

YoutubeDownloadFromTitle(title)
{
  Run, youtube-dl ytsearch:"%title%",,Minimize   
}

; Use whatever is on Clipboard on youtube-dl to search for similar tracks
^!x::
{
  Run youtube-dl ytsearch:"""%clipboard%""",,Minimize
  Sleep, 10
  return
}

;Next Track on Youtube
^!n::SendToYoutube("+n")

;Previous Track on Youtube
^!b::SendToYoutube("+p")

;Pause/Play Track on Youtube
^!p::SendToYoutube("+{space}")

; Function to send commands to Youtube
SendToYoutube(command)
{
  WinGet, winid ,, A ; <-- need to identify window A = acitive
  ; MsgBox, winid=%winid% 


  IfWinExist, ahk_class Chrome_WidgetWin_1 
      WinActivate, ahk_class Chrome_WidgetWin_1 ;, Why I Ducking Hate Autocorrect - YouTube - Google Chrome  ; use the window found above
  ;     ControlFocus, Chrome_RenderWidgetHostHWND1, Downloads - Google Chrome

  prev = %Clipboard% 

  SetTitleMatchMode, 2
  WinWaitActive - Google Chrome
  ControlFocus, Chrome_RenderWidgetHostHWND1

  counter := 0
  title :=""
  address :=""
  found:= false
  Loop, 100
  {
     WinGetTitle, Title, A  ;get active window title
     if(InStr(Title, " - Youtube")>0)
     {
        title := Title
        found := true

        Send %command%
        Send !d ; Select the url textbox
        Sleep, 5
        Send ^c ; Copy the urlSent 
        address = %Clipboard%        
        Sleep, 5
        Send, {f6}
        Sleep, 5
        Send {f6}
        break
     }
     counter += 1
     Send ^{Tab}
     Sleep, 5
  }
  ; Send, counter
  Loop, %counter%
  {
    Send ^+{Tab}
    Sleep, 5 
  }

  WinActivate ahk_id %winid% ; <-- correct
  Clipboard = %prev%

  return, [found, title, address]
}

^!d::GetVideoNameYoutube()

; Get The name of the video playing on YouTube
GetVideoNameYoutube()
{
  results := SendToYoutube("")

  found := results[1]
  title := results[2]
  address := results[3]

  title := StrReplace(title, "- YouTube - Google Chrome", "")
  Gui, +AlwaysOnTop +Disabled -SysMenu +Owner  ; +Owner avoids a taskbar button.
  Gui, Add, Text,, %title%
  Gui, Show, NoActivate,  ; NoActivate avoids deactivating the currently active window.
  Sleep, 2000
  Gui Destroy
}


